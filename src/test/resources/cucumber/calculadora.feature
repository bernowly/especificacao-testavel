# language: pt

Funcionalidade: Efetuar operações matemáticas simples

  Cenário: Somar dois números
    Dado que eu tenho uma calculadora
    Quando eu adiciono 20 e 35
    Então o resultado da soma deve ser 55

  Cenário: Multiplicar dois números
    Dado que eu tenho uma calculadora
    Quando eu multiplico 12 por 5
    Então o resultado da multiplicação deve ser 60

