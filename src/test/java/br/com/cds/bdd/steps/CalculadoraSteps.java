package br.com.cds.bdd.steps;

import br.com.cds.bdd.calculadora.Calculadora;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Quando;
import cucumber.api.java.pt.Entao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class CalculadoraSteps {

    private Calculadora calculadora;

    @Dado("^que eu tenho uma calculadora$")
    public void inicializarCalculadora() throws Throwable {
        calculadora = new Calculadora();
        assertNotNull(calculadora);
    }

    @Quando("^eu adiciono (\\d+) e (\\d+)$")
    public void somar(int numero1, int numero2) throws Throwable {
        calculadora.somar(numero1, numero2);
    }

    @Entao("^o resultado da soma deve ser (\\d+)$")
    public void verificarResultadoSoma(int resultado) throws Throwable {
        assertEquals(resultado, calculadora.obterResultado());
    }


    @Quando("^eu multiplico (\\d+) por (\\d+)$")
    public void multiplicar(int numero1, int numero2) throws Throwable {
        calculadora.multiplicar(numero1, numero2);
    }

    @Entao("^o resultado da multiplicação deve ser (\\d+)$")
    public void verificarResultadoMultiplicacao(int resultado) throws Throwable {
        assertEquals(resultado, calculadora.obterResultado());
    }

}


