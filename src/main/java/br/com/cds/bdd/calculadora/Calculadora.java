package br.com.cds.bdd.calculadora;

public class Calculadora {

    private int resultado;

    public void somar(int numero1, int numero2) {
        resultado = numero1 + numero2;
    }

    public void multiplicar(int numero1, int numero2) {
        resultado = numero1 * numero2;
    }

    public int obterResultado() {
        return resultado;
    }

}

